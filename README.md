# The News Hub tech-challenge

The exercise is to create a full stack JavaScript solution - node.js and express.js(version 4+) on the backend with ember.js on the client.

Here is an outline of what we are looking for:

1. Use node.js/express.js to send the circles.json to a request made by ember.js
2. Parse the json response and create the appropriate models (we make no restrictions here)
3. Plot the 4 parent inner circles (you can assume a radius of 100px), and draw the smaller circles (you can assume a radius of 50px) around it.
	- Your code must figure out the number of circles that can fit into an outer circles (we will scale the array size to test) and plot them.
	- After the circles are drawn please allow for a click action that will change the background of the clicked circle and the parent inner circle.


Please time yourself during this exercise and include the time taken to complete it.


## Setup

1. git clone https://bitbucket.org/ben-crowhurst/the-news-hub-technical-test.git
2. cd the-news-hub-technical-test
3. npm install
5. npm start
4. http://localhost:3000/




## Useful References:

https://www.illustrativemathematics.org/illustrations/710

http://en.wikipedia.org/wiki/Circle_packing

http://math.stackexchange.com/questions/793691/visual-illustrations-of-circle-packing-theorem