var express = require('express');
var router = express.Router();

var circle_data = require(process.cwd() + '/data/circles');
/* GET cirlces listing. */
router.get('/', function(req, res) {
  res.send(circle_data);
});

module.exports = router;
