Application.CircleView = Ember.View.extend({
  templateName: 'circle',
  innerDiameter: 100,
  outerDiameter: 50,
  centreCircleElementId: 'inner-circle',
  innerRadius: function() {
    var view = this;
    return view.get('innerDiameter') / 2;
  }.property('innerDiameter'),
  outerRadius: function() {
    var view = this;
    return view.get('outerDiameter') / 2;
  }.property('outerDiameter'),
  didInsertElement: function() {
  	var view = this;
  	var circle = view.get('content');
  	var photos = circle.get('photos').toArray();

    var innerRadius = view.get('innerRadius');
    var outerRadius = view.get('outerRadius');
    var imaginaryRadius = innerRadius;

    for (var count = 0; count <= photos.length;) {
      var limit = view.determineOuterCircleLimit(imaginaryRadius, outerRadius);

	  view.renderPhotos(photos.slice(count), limit);

      imaginaryRadius += outerRadius;
	  count += limit;
    }
  },
  determineOuterCircleLimit: function(innerRadius, outerRadius) {
  	var view = this;
	var radius = innerRadius + outerRadius;
	var diameter = radius * 2;
	var circumference = Math.PI * diameter;

	var limit = Math.floor(circumference / view.get('outerDiameter'));
	return limit;
  },
  determineXcoordinate: function(index, limit) {
  	var view = this;
  	var innerRadius = view.get('innerRadius');
    var outerRadius = view.get('outerRadius');
  	var padding = limit * limit;
  	var radius = innerRadius - outerRadius;

    return (radius + padding * Math.cos(2 * Math.PI * index / limit));
  },
  determineYcoordinate: function(index, limit) {
  	var view = this;
  	var innerRadius = view.get('innerRadius');
    var outerRadius = view.get('outerRadius');
  	var padding = limit * limit;
  	var radius = innerRadius - outerRadius;

    return (radius + padding * Math.sin(2 * Math.PI * index / limit));
  },
  renderPhotos: function(photos, limit) {
  	var view = this;
  	var innerRadius = view.get('innerRadius');
    var outerRadius = view.get('outerRadius');

	for(var index = 0; index < photos.length; index++) {
	  var x = view.determineXcoordinate(index, limit);
	  var y = view.determineYcoordinate(index, limit);
	 
      var photo = photos[index];
	  view.renderCircularPhoto(x, y, photo.get('src'));   
	}
  },
  renderCircularPhoto: function(x, y, src) {
  	var  view = this;
  	var element = $('#' + view.get('centreCircleElementId'));

  	element.append("<div class='outer-circle' style='left:"+ x +"px;top:"+ y +"px;background:url(" + src + ") no-repeat'></div>");
  },
  click: function(evt) {
  	var view = this;
  	var centreCircleElement = $('#' + view.get('centreCircleElementId'));
  	var sateliteCircleElement = $(evt.target);
    var background = centreCircleElement.css('background');

  	centreCircleElement.css('background', sateliteCircleElement.css('background'));
  	sateliteCircleElement.css('background', background);
  }
});
